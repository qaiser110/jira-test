# Automated Tests for JIRA issues - Add/Edit/Search pages
Submitted by: Qaiser Abbas [![LinkedIn](http://alylabs.com/LinkedIn.png)](https://www.linkedin.com/in/qabbas "Qaiser on LinkedIn")
____________________________________________________________________________

# Introduction

The repository contains automated tests for creating, editing, and searching for issues Atlassian's JIRA using Selenium 2.0 aka [WebDriver](http://docs.seleniumhq.org/projects/webdriver/) using the [Page Objects Pattern](http://code.google.com/p/selenium/wiki/PageObjects).

The tests were run on Atlassian’s [public instance](https://jira.atlassian.com/browse/TST).

The tests should verify that:
 * New issues can be created
 * Existing issues can be updated
 * Existing issues can be found via JIRA’s search

It should be noted that the tests are only for demonstration purpose, and only cover some simple tests.

## Environment 

**Selenium Version:** Latest (2.44.0)

**Selenium Driver:** FirefoxDriver

**Language:** Java

**IDE:** Eclipse


# Assumptions

 * It is assumed that there is already a working setup for Selenium. If not, the instructions for setting up Selenium are given below (with Eclipse IDE).

 * The credentials used for the login may be modified from the following location: `src/common/LoginPage.java`

 * The possible number of tests could be endless. So it is assumed that the automated tests are just required for a demonstration of the ability and not the possibilities. The possibilities are always endless.

 * The tests for issue creation/modification only include the testing for the required field (Issue __Summary__ field).

 * I've created a new issue in each test, to be certain that the issue will exist in the system. But in the actual test environment, it would be better to use some existing test data to make test faster. 

 * The code needs a lot of refactoring.